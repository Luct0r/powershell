# powershell

There's so much to learn in this field and PowerShell is definitely important both offensively and defensively.
I'll probably never be an expert, but the only way to improve is by doing...

Some notes on setting up a PowerShell profile:

Check if your profile exists or not.

`Test-Path $PROFILE.CurrentUserAllHosts`

If Test-Path returns false...create one.

`New-Item -Type File -Path $PROFILE.CurrentUserAllHosts -Force`

To edit your profile.

`PowerShell_ISE.exe $PROFILE.CurrentUserAllHosts`

The profile I have pretty much just goes in that "Scripts" directory and loads whatever it finds there.

```
# My PowerShell Profile
# Automatically load these scripts for use in PowerShell.

$PSDir = "C:\Users\$env:UserName\Documents\WindowsPowerShell\Scripts"

Get-ChildItem "${psdir}\*.ps1" | %{.$_} 

Write-Host "Loading custom scripts/functions..." -ForegroundColor Cyan
Write-Host
Write-Host "Custom PowerShell Environment Loaded" -ForegroundColor Cyan
Write-Host
```